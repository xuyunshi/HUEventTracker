#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "HUEventConfig.h"
#import "HUEventObject.h"
#import "HUEventTracker.h"
#import "UIResponder+HUEvent.h"

FOUNDATION_EXPORT double HUEventTrackerVersionNumber;
FOUNDATION_EXPORT const unsigned char HUEventTrackerVersionString[];

