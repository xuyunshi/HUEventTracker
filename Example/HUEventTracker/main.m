//
//  main.m
//  HUEventTracker
//
//  Created by huluobo on 08/16/2018.
//  Copyright (c) 2018 huluobo. All rights reserved.
//

@import UIKit;
#import "HUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HUAppDelegate class]));
    }
}
