//
//  HUViewController.m
//  HUEventTracker
//
//  Created by huluobo on 08/16/2018.
//  Copyright (c) 2018 huluobo. All rights reserved.
//

#import "HUViewController.h"
#import <HUEventTracker/HUEventTracker.h>

@interface HUViewController ()

@end

@implementation HUViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonClicked:(UIButton *)sender {
    sender.eventID = @"0001";
}

@end
