//
//  HUAppDelegate.h
//  HUEventTracker
//
//  Created by huluobo on 08/16/2018.
//  Copyright (c) 2018 huluobo. All rights reserved.
//

@import UIKit;

@interface HUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
