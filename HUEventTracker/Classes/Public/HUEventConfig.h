//
//  HUEventConfig.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HUEventConfig : NSObject

/**
 *  设置自定义渠道标识
 */
@property (nonatomic, copy) NSString *channel;

/**
 *  设置自定义版本号
 */
@property (nonatomic, copy) NSString *version;


/**
 *  设置自定义运行环境
 */
@property (nonatomic, copy) NSString *platform;


/**
 *  业务线
 */
@property (nonatomic, copy) NSString *businessLine;

/**
 *  业务版本
 */
@property (nonatomic, copy) NSString *businessVersion;


/**
 * 自定义数据
 */
@property (nullable, nonatomic, copy) NSDictionary *customData;

@end
