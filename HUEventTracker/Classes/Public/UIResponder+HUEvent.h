//
//  UIResponder+Event.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/11.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <UIKit/UIKit.h>

UIViewController* findTheViewController(id target);

@class HUEventObject;
@interface UIResponder (HUEvent)

// 事件 ID，需唯一
@property (nonatomic, copy, nullable) NSString *eventID;

@property (nonatomic, strong, nullable) HUEventObject *eventObj;

@end
