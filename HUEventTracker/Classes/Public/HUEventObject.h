//
//  HUEvent.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HUEventConfig;
@interface HUEventObject : NSObject
/**
 * 事件 ID
 */
@property (nullable, nonatomic, copy) NSString *eventID;


/**
 *  当前事件对象 ID
 */
@property (nullable, nonatomic, copy) NSString *objectID;

/**
 * 事件触发时间，时间戳(秒)
 */
@property (nonatomic) NSInteger timestamp;

/**
 * 事件类型
 */
@property (nonatomic, copy) NSString *eventType;

/**
 * 拓展数据
 */
@property (nullable, nonatomic, copy) NSDictionary *extraData;

/**
 * 自定义配置
 */
@property (nonatomic, strong) HUEventConfig *config;

/**
 *  页面 ID
 */
@property (nullable, nonatomic, copy) NSString *pageID;

/**
 *  用户 ID
 */
@property (nonnull, nonatomic, copy) NSString *userID;

/**
 *  用户所属于哪个客户端
 */
@property (nonnull, nonatomic, copy) NSString *clinet;

/**
 * 自定义数据
 */
@property (nullable, nonatomic, copy) NSDictionary *customData;

@end


@interface HUEventObject (JSON)

- (NSDictionary *)jsonData;

@end

@interface HUPageEventObject: HUEventObject

/**
 *   上个页面 ID
 */
@property (nullable, nonatomic, copy) NSString *lastPageID;

@end
