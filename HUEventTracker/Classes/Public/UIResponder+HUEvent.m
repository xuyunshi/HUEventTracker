//
//  UIResponder+Event.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/11.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UIResponder+HUEvent.h"
#import <objc/runtime.h>
#import "HUEventTracker+Extension.h"
#import "HUEventObject.h"


const void *eventIDKey = "eventIDKey";

const void *eventObjKey = "eventObjKey";

@implementation UIResponder (HUEvent)

- (void)setEventID:(nullable NSString *)eventID {
    objc_setAssociatedObject(self, eventIDKey, eventID, OBJC_ASSOCIATION_COPY);
}

- (nullable NSString *)eventID {
    id event = objc_getAssociatedObject(self, eventIDKey);
    if (!event) {
        event = [[HUEventTracker sharedInstance] pageIDForPage:NSStringFromClass(self.class)];
    }
    return event;
}

- (void)setEventObj:(HUEventObject *)eventObj {
    self.eventID = eventObj.eventID;
    objc_setAssociatedObject(self, eventObjKey, eventObj, OBJC_ASSOCIATION_RETAIN);
}

- (HUEventObject *)eventObj {
    return objc_getAssociatedObject(self, eventObjKey);
}

@end

UIViewController* findTheViewController(id target) {
    if (![target isKindOfClass:UIResponder.class]) {
        return nil;
    }
    UIResponder *_responser = (UIResponder *)target;
    while (_responser) {
        if ([_responser isKindOfClass:UIViewController.class]) {
            return (UIViewController *)_responser;
        }
        _responser = _responser.nextResponder;
    }
    return nil;
}
