//
//  UITableView+Event_MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/11.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UITableView+Event_MethodSwizzing.h"
#import "NSObject+MethodSwizzing.h"

@implementation UITableView (Event_MethodSwizzing)

//+ (void)load {
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        [self exchangeOriginalSelector:@selector(setDelegate:)
//                  withSwizzingSelector:@selector(swizzing_setDelegate:)];
//    });
//}

- (void)swizzing_setDelegate:(id<UITableViewDelegate>)delegate {
    
    [self swizzing_setDelegate:delegate];
}

@end
