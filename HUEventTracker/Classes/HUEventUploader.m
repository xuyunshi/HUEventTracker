//
//  HUEventUploader.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "HUEventUploader.h"
#import "HUEventUploadOperation.h"
#import "HUEventCollector.h"

@interface HUEventUploader() {
    HUEventObject *_event;
    NSMutableDictionary *_uploadOperations;
}

@property (nonatomic, strong) NSOperationQueue *uploadQueue;

@end

@implementation HUEventUploader

+ (instancetype)sharedInstance {
    static HUEventUploader *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    return instance;
}

- (void)uploadEvent:(HUEventObject *)event {
    HUEventUploadOperation *operation = [[HUEventUploadOperation alloc] initWithEvent:event uploadCompletion:^(HUEventObject *event, NSError *error) {
        if (error) {
            NSLog(@"error:%@ \n", error);
            return ;
        }
        [[HUEventCollector sharedInstance] removeEvent:event];
    }];
    [self.uploadQueue addOperation:operation];
}

- (NSOperationQueue *)uploadQueue {
    if (_uploadQueue == nil) {
        _uploadQueue = [[NSOperationQueue alloc] init];
        _uploadQueue.maxConcurrentOperationCount = 5;
    }
    return _uploadQueue;
}

@end
