//
//  HUEventTracker+Extension.m
//  HUEventTracker
//
//  Created by huluobo on 2018/8/17.
//

#import "HUEventTracker+Extension.h"

@implementation HUEventTracker (Extension)

- (nullable NSString *)pageIDForPage:(NSString *)page {
    NSArray *s = [page componentsSeparatedByString:@"."];
    NSString *key = page;
    if (s.count > 1) {
        key = [s lastObject];
    }
    return [_pageMap valueForKey:key];
}

@end
