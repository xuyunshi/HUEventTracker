//
//  NSObject+MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/10.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "NSObject+MethodSwizzing.h"
#import <objc/message.h>

void exchangeSelector(Class, SEL, SEL);

@implementation NSObject (MethodSwizzing)

+ (void)exchangeOriginalSelector:(SEL)original withSwizzingSelector:(SEL)swizzing {
    exchangeSelector(self, original, swizzing);
}

@end


void exchangeSelector(Class cls, SEL original, SEL swizzing) {
    
    Method originalMethod = class_getInstanceMethod(cls, original);
    Method swizzingMethod = class_getInstanceMethod(cls, swizzing);
    
    BOOL didAddMethod = class_addMethod(cls,
                                        original,
                                        method_getImplementation(swizzingMethod),
                                        method_getTypeEncoding(swizzingMethod));
    if (didAddMethod) {
        class_replaceMethod(cls,
                            swizzing,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzingMethod);
    }
}
