//
//  UIGestureRecognizer+MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/11.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UIGestureRecognizer+Event_MethodSwizzing.h"
#import "NSObject+MethodSwizzing.h"

@implementation UIGestureRecognizer (Event_MethodSwizzing)

//+ (void)load {
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        [self exchangeOriginalSelector:@selector(initWithTarget:action:)
//                  withSwizzingSelector:@selector(swizzing_initWithTarget:action:)];
//        
//        [self exchangeOriginalSelector:@selector(addTarget:action:)
//                  withSwizzingSelector:@selector(swizzing_addTarget:action:)];
//        
//    });
//}

- (instancetype)swizzing_initWithTarget:(id)target action:(SEL)action {
    NSLog(@"\n\n+++++++++++++++++ UIGestureRecognizer +++++++++++++++++++++++");
    NSLog(@"action: %@", NSStringFromSelector(action));
    NSLog(@"target: %@", target);
    NSLog(@"+++++++++++++++++ UIGestureRecognizer +++++++++++++++++++++++\n\n");
    
    
    return [self swizzing_initWithTarget:target action:action];
}

- (void)swizzing_addTarget:(id)target action:(SEL)action {
    NSLog(@"\n\n+++++++++++++++++ UIGestureRecognizer +++++++++++++++++++++++");
    NSLog(@"action: %@", NSStringFromSelector(action));
    NSLog(@"target: %@", target);
    NSLog(@"+++++++++++++++++ UIGestureRecognizer +++++++++++++++++++++++\n\n");
    
    return [self swizzing_addTarget:target action:action];
}

@end
