//
//  NSObject+MethodSwizzing.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/10.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MethodSwizzing)

+ (void)exchangeOriginalSelector:(SEL)original withSwizzingSelector:(SEL)swizzing;

@end
