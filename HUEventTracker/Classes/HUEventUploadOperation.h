//
//  HUEventUploadOperation.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>


@class HUEventObject;

typedef void(^UploadEventCompletion)(HUEventObject * _Nonnull event, NSError * _Nullable error);

@interface HUEventUploadOperation : NSOperation

- (instancetype)initWithEvent:(nonnull HUEventObject *)event uploadCompletion:(UploadEventCompletion)completion;

@end
