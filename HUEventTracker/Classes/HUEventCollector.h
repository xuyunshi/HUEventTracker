//
//  HUEventCollector.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HUEventObject.h"

@interface HUEventCollector : NSObject

+ (nonnull instancetype)sharedInstance;


@property (nonatomic, strong, readonly) NSArray<HUEventObject *> *allEvents;

/**
 *  收集事件
 */
- (void)collectEvent:(nonnull HUEventObject *)event;

- (void)removeEvent:(nonnull HUEventObject *)event;

@end
