//
//  UIViewController+Event_MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UIViewController+Event_MethodSwizzing.h"
#import "NSObject+MethodSwizzing.h"
#import "UIResponder+HUEvent.h"
#import "HUEventTracker.h"
#import "HUEventObject.h"

@implementation UIViewController (Event_MethodSwizzing)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self exchangeOriginalSelector:@selector(viewDidAppear:)
                  withSwizzingSelector:@selector(swizzing_viewDidAppear:)];

        [self exchangeOriginalSelector:@selector(viewDidDisappear:)
                  withSwizzingSelector:@selector(swizzing_viewDidDisappear:)];

    });
}

- (void)swizzing_viewDidAppear:(BOOL)animated {
    if (self.eventID) {
        // 找到上个控制器
        UIViewController *lastVC = nil;
        if (self.navigationController) {
            if (self.navigationController.viewControllers.count > 1) {
                lastVC = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            }
        } else {
            lastVC = self.presentingViewController;
        }
        
        HUPageEventObject *event = [HUPageEventObject new];
        event.extraData = self.eventObj.extraData;
        event.eventType = @"page_init";
        event.pageID = self.eventID;//[[HUEventTracker sharedInstance] pageIDForPage:NSStringFromClass(self.class)];
        event.lastPageID = lastVC.eventID;//[[HUEventTracker sharedInstance] pageIDForPage:NSStringFromClass(lastVC.class)];
        
        [[HUEventTracker sharedInstance] collectEvent:event];
    }
    
    [self swizzing_viewDidAppear:animated];
}

- (void)swizzing_viewDidDisappear:(BOOL)animated {
    if (self.eventID) {
        // 找到上个控制器
        UIViewController *lastVC = nil;
        if (self.navigationController) {
            if (self.navigationController.viewControllers.count > 1) {
                lastVC = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            }
        } else {
            lastVC = self.presentingViewController;
        }
        
        HUPageEventObject *event = [HUPageEventObject new];
        event.extraData = self.eventObj.extraData;
        event.eventType = @"page_exit";
        //event.objectID = self.eventID;
        event.pageID = self.eventID;//[[HUEventTracker sharedInstance] pageIDForPage:NSStringFromClass(self.class)];
        event.lastPageID = lastVC.eventID;//[[HUEventTracker sharedInstance] pageIDForPage:NSStringFromClass(lastVC.class)];
        
        [[HUEventTracker sharedInstance] collectEvent:event];
    }
    [self swizzing_viewDidAppear:animated];
}

@end
