# HUEventTracker

[![CI Status](https://img.shields.io/travis/huluobo/HUEventTracker.svg?style=flat)](https://travis-ci.org/huluobo/HUEventTracker)
[![Version](https://img.shields.io/cocoapods/v/HUEventTracker.svg?style=flat)](https://cocoapods.org/pods/HUEventTracker)
[![License](https://img.shields.io/cocoapods/l/HUEventTracker.svg?style=flat)](https://cocoapods.org/pods/HUEventTracker)
[![Platform](https://img.shields.io/cocoapods/p/HUEventTracker.svg?style=flat)](https://cocoapods.org/pods/HUEventTracker)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HUEventTracker is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HUEventTracker'
```

## Author

huluobo, hujewelz@163.com

## License

HUEventTracker is available under the MIT license. See the LICENSE file for more info.
